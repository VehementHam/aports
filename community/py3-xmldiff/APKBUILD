# Contributor: Sascha Brawer <sascha@brawer.ch>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=py3-xmldiff
pkgver=2.6.3
pkgrel=2
pkgdesc="Creates diffs of XML files"
url="https://github.com/Shoobx/xmldiff"
arch="noarch"
license="MIT"
depends="py3-lxml"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/x/xmldiff/xmldiff-$pkgver.tar.gz"
builddir="$srcdir/xmldiff-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
ba25b8cc9866c66bc829a82d1f20108c17a09e2f46735763317f7fd6f60c3920ba6d3114454ead1461de4a59c177065291bb71bebb7ee9c4a03f3584452a9e33  xmldiff-2.6.3.tar.gz
"
