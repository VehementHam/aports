# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-msoffcrypto-tool
pkgver=5.4.0
pkgrel=0
pkgdesc="tool and library for decrypting MS Office files with passwords or other keys"
url="https://github.com/nolze/msoffcrypto-tool"
arch="noarch"
license="MIT"
depends="python3 py3-cryptography py3-olefile py3-setuptools"
makedepends="py3-gpep517 py3-installer py3-poetry-core"
checkdepends="bash py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/nolze/msoffcrypto-tool/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/msoffcrypto-tool-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	(
		# shellcheck disable=1091
		. .testenv/bin/activate
		.testenv/bin/python3 -m pytest
	)
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
bebb727754512b0f1f011538d65f113d0b63c59b758fc5d427202c74587f9efc091cf5747098f6b50df40761a143ba8732a9a5e253827bcffb5f5577af986a0c  py3-msoffcrypto-tool-5.4.0.tar.gz
"
